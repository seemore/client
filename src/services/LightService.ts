import { exec, ChildProcess } from 'child_process'

export class LightService {
    private static demoProcess: ChildProcess | null

    public static startDemo() {
        const newProcess: ChildProcess | null = exec(
            'sudo $(which python) src/hardware/demo.py',
            (error, stdout, stderr) => {
                if (error) {
                    console.error(`exec error: ${error}`)
                    return
                }
                console.log(`stdout: ${stdout}`)
                console.error(`stderr: ${stderr}`)
            }
        )

        this.demoProcess = newProcess
    }

    public static stopDemo() {
        if (this.demoProcess) {
            exec(`sudo pkill -TERM -P ${this.demoProcess.pid}`)
            console.log('Process canceled')
            this.demoProcess = null
        }
    }

    public static setColor({ color }: { color: string }): void {
        exec(
            `sudo $(which python) src/hardware/color.py "${color}"`,
            (error, stdout, stderr) => {
                if (error) {
                    console.error(`exec error: ${error}`)
                    return
                }
                console.log(`stdout: ${stdout}`)
                console.error(`stderr: ${stderr}`)
            }
        )
    }

    public static cleanup(): void {
        // TODO: Implement cleanup
    }
}
