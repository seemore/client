/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */

const PIN_SERVO = 17

export class ServoService {
    private static servo: any = null // Use 'any' to support both real and mock versions

    public static async init(): Promise<void> {
        if (process.platform !== 'linux') {
            console.log('Not on Linux, skipping pigpio initialization.')
            return
        }

        try {
            const { Gpio } = await import('pigpio')
            this.servo = new Gpio(PIN_SERVO, { mode: Gpio.OUTPUT })
        } catch (error) {
            console.warn(
                'pigpio module is not available, running without servo control.'
            )
        }
    }

    // Set the servo position (0.0 to 1.0)
    public static setPosition({ position }: { position: number }): void {
        if (position < 0.0 || position > 1.0) {
            console.warn(
                `WARNING: Position ${position * 100}% is out of bounds, clamping to valid range [0.0, 1.0]`
            )
        }

        // Clamp the input position between 0.0 and 1.0
        const clampedInput = Math.max(0.0, Math.min(1.0, position))
        const pulseWidth = 1000 + clampedInput * 1000 // Convert to pulse width (1000 to 2000)
        console.log(
            `Moving servo to ${clampedInput * 100}% [Pulse Width: ${pulseWidth}]`
        )

        // Ensure the servo is initialized before using it
        if (!this.servo) {
            console.warn(
                'WARNING: Servo not initialized, running in simulation mode.'
            )
            return
        }

        // Move the servo (real or simulation)
        this.servo.servoWrite(pulseWidth)
    }

    // Stops the servo by setting pulse width to 0
    public static cleanup(): void {
        console.log('Stopping servo and cleaning up...')
        this.setPosition({ position: 0.0 })
    }
}
