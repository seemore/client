/* eslint-disable @typescript-eslint/no-explicit-any */
import si from 'systeminformation'
import { execSync } from 'child_process'
import * as os from 'os'
import * as path from 'path'
import * as fs from 'fs'

export class DiagnosticService {
    public static async heartbeatDiagnostics(): Promise<any> {
        const cpuLoad = (await si.currentLoad()).currentLoad / 100
        const memory = await si.mem()
        const memoryUsage = {
            used: memory.used / 1024 ** 2,
            total: memory.total / 1024 ** 2,
        }
        const disk = await si.fsSize()
        const diskUsage = {
            used: disk[0].used / 1024 ** 2,
            size: disk[0].size / 1024 ** 2,
        }
        const temp = (await si.cpuTemperature()).main

        return { cpuLoad, memoryUsage, diskUsage, temp }
    }

    public static async initialProperties(): Promise<any> {
        // const cpu = await si.cpu()
        // const osInfo = await si.osInfo()
        // const memory = await si.mem()
        // const diskLayout = await si.diskLayout()
        // const networkInterfaces = await si.networkInterfaces()

        const uptime = (await si.time()).uptime
        const gitLastPullTimestamp = this.getLastGitPullTime()

        return { diagnostics: { uptime }, gitLastPullTimestamp }
    }

    public static getLastGitPullTime(repoPath: string = '.'): Date | undefined {
        // Define the path to .git/FETCH_HEAD
        const fetchHeadPath = path.join(repoPath, '.git', 'FETCH_HEAD')

        // Check if FETCH_HEAD exists
        if (!fs.existsSync(fetchHeadPath)) {
            console.log(
                'FETCH_HEAD not found. This may indicate that the repo has never been pulled.'
            )
            return undefined
        }

        try {
            // Determine the OS and set the correct command
            const platform = os.platform()
            let command: string

            if (platform === 'darwin') {
                // macOS
                command = `stat -f "%Sm" -t "%Y-%m-%d %H:%M:%S" ${fetchHeadPath}`
            } else if (platform === 'linux') {
                // Linux (including Raspberry Pi)
                command = `stat --format="%y" ${fetchHeadPath}`
            } else {
                console.log('Unsupported OS for this function.')
                return undefined
            }

            // Execute the command to get the modification time
            const lastPullTimeStr = execSync(command).toString().trim()

            // Convert the result string to a Date object
            const lastPullTime = new Date(lastPullTimeStr)
            return isNaN(lastPullTime.getTime()) ? undefined : lastPullTime
        } catch (error) {
            console.error('Error retrieving last pull time:', error)
            return undefined
        }
    }
}
