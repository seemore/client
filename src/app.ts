import './socket' // This will initialize your Socket.IO client and event listeners
import { ServoService } from './services/ServoService'

async function startClient() {
    console.log('Socket.IO client started')

    // Setup Servo Service
    await ServoService.init()
    ServoService.setPosition({ position: 0.0 })
}

startClient().catch((err) => {
    console.error('Error starting the server:', err)
})
