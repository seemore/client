import time
import random
from rpi_ws281x import Adafruit_NeoPixel, Color
import argparse

# LED strip configuration:
LED_COUNT      = 32      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 16    # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

# Create NeoPixel object with appropriate configuration.
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
# Intialize the library (must be called once before other functions).
strip.begin()

def set_strip_color(strip, color):
    """Set the entire strip to a specific color."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
    strip.show()


def parse_color(color_str):
    """Parse a color string in the format 'R,G,B' into a Color object."""
    hex_color = color_str.lstrip('#')
    
    if len(hex_color) != 6:
        raise ValueError("Invalid color string")
    
    r = int(hex_color[0:2], 16)
    g = int(hex_color[2:4], 16)
    b = int(hex_color[4:6], 16)
    
    return Color(r, g, b)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Set the color of an LED strip.')
    parser.add_argument('color', help="The color to set the strip to, in the format 'R,G,B'.")
    args = parser.parse_args()

    color = parse_color(args.color)
    set_strip_color(strip, color)
