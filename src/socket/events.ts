import { Socket } from 'socket.io-client'
import { LightService } from '../services/LightService'
import { ServoService } from '../services/ServoService'
import { DiagnosticService } from '../services/DiagnosticService'

export const setupEventListeners = (socket: Socket): void => {
    // Connection
    socket.on('connect', async () => {
        console.log('Connected to server!')
        const initialProps = await DiagnosticService.initialProperties()
        socket.emit('diagnostics:heartbeat', initialProps)
    })

    socket.on('clients:identify', () => {
        LightService.setColor({ color: '#24ff00' })
        ServoService.setPosition({ position: 1.0 })

        // After 3 seconds, reset servo position and turn off the light
        setTimeout(() => {
            ServoService.setPosition({ position: 0.0 })
            LightService.setColor({ color: '#000000' })
        }, 3000) // 3000ms = 3 seconds
    })

    // Lights
    socket.on('light:color', (color) => {
        LightService.setColor({ color: color })
    })
    socket.on('light:demo:start', LightService.startDemo)
    socket.on('light:demo:stop', LightService.stopDemo)

    // SERVO EVENTS
    socket.on('servo:position', (position) => {
        ServoService.setPosition({ position: position })
    })

    // DIAGNOSTICS EVENTS
    async function sendDiagnostics() {
        const heartbeat = await DiagnosticService.heartbeatDiagnostics()
        socket.emit('diagnostics:heartbeat', { diagnostics: heartbeat })
    }

    setInterval(sendDiagnostics, 5000)

    socket.on('diagnostics:comprehensive', (callback) => {
        // TODO: Add more diagnostics data
        callback({})
    })

    // Cleanup
    socket.on('disconnect', () => {
        console.log('Disconnected from server')
        // LightService.cleanup();
    })
}
