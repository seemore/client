import { io } from 'socket.io-client'
import { setupEventListeners } from './events'
import os from 'os'
import dotenv from 'dotenv'

dotenv.config()

// Get the device ID and hostname
const deviceId = process.env.ID || -1
const hostname = os.hostname()

//TODO: Set this to the dev server's IP/Hostname
const serverIP = 'http://192.168.0.225:4000'

const socket = io(serverIP, {
    query: {
        clientID: deviceId,
        hostname: hostname,
        type: 'client',
    },
})
setupEventListeners(socket)

export default socket
