import { spawn } from "child_process"
import { Socket } from "socket.io-client"

type LogEntry = {
    timestamp: string,
    message: string,
}

export const startLogging = (socket: Socket): void => {

    // Runs command to continuously log
    const loggingProccess = spawn("journalctl", ["-f", "-o", "json"], { stdio: ["ignore", "pipe", "pipe"] });

    loggingProccess.stdout.on("data", (data) => {
        const logEntries = data.toString().trim().split("\n")
        
        logEntries.forEach((entry: string) => {
            try {
                const logJSONObject = JSON.parse(entry)
                
                // Converts to readable time
                const milliseconds = Number(logJSONObject.__REALTIME_TIMESTAMP) / 1000;
                const timestamp = new Date(milliseconds).toISOString();
                const message = logJSONObject.MESSAGE

                // Only sends the timestamp and the message
                const log: LogEntry = {
                    timestamp: timestamp,
                    message: message,
                }

                socket.emit("log", log)
            } catch (error) {
                console.error("Error parsing journalctl JSON:", error);
            }
        })
    })

    loggingProccess.stderr.on("data", (data) => {
        console.error("Error capturing logs:", data.toString())
    })

    loggingProccess.on("close", (code) => {
        console.log(`Log capture process exited with code ${code}`)
    })
}