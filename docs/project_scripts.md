## Project Scripts

This section outlines the key scripts for development, code quality enforcement, and project maintenance.

### 1. **Start Development Environment**
```bash
npm run dev
```
**Usage:**  
Activates the Python virtual environment and runs the project in development mode using `ts-node`. This allows real-time execution of your TypeScript code from `src/index.ts` without needing to compile manually.

---

### 2. **Run Code Linter**
```bash
npm run lint
```
**Usage:**  
Executes ESLint on the project to identify syntax issues and enforce code style based on your ESLint configuration.

---

### 3. **Auto-Format Codebase**
```bash
npm run format
```
**Usage:**  
Formats all files in the project according to Prettier’s code style guidelines, ensuring consistency and clean code across the codebase.

---

### 4. **Check Code Formatting**
```bash
npm run format:check
```
**Usage:**  
Checks if the code is formatted according to Prettier rules. It reports any formatting issues but doesn't modify files.

---

### 5. **Clean Up Project Dependencies**
```bash
npm run clean
```
**Usage:**  
Deletes the `node_modules` directory, removing all installed dependencies. Useful for resetting the project environment or resolving dependency issues.

---

### 6. **Reinstall Project Dependencies**
```bash
npm run reinstall
```
**Usage:**  
Runs the `clean` script to remove `node_modules` and then reinstalls all dependencies using `npm install`, ensuring a fresh environment.
