# Installation Guide

1. Install Python
First, ensure that Python 3 is installed, as it's required for various dependencies.

If on a Mac, install python using homebrew. This will install venv and pip.
```bash
brew install python
```
If on Windows, install python directly from their website.

2. Set Up a Virtual Environment
A virtual environment helps isolate dependencies, preventing conflicts with other projects.

```bash
python3 -m venv venv
```

You can activate the virtual envionment with
```bash
source venv/bin/activate
```

After this, you should see (venv) at the beginning of your command line, indicating the virtual environment is active.

3. Install Node.js and npm
Install Node.js, which includes npm (Node Package Manager), required for managing JavaScript packages.

This can be installed from their website.

To verify the installation, check the versions:

```bash
node -v
npm -v
```
Note: If you need a specific version, you may use nvm (Node Version Manager) to install and switch between Node.js versions.

4. Install Dependencies from npm
In the root of your project, install the JavaScript dependencies listed in package.json:

```bash
npm install
```

5. Install Python Dependencies from pip
Now, install the Python dependencies specified in requirements.txt. Make sure you're still in the virtual environment ((venv) should appear in the terminal prompt).

```bash
pip install -r requirements.txt
```
