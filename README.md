# SeeMore II Client

## Getting Started 

To compile and start the client:

```bash
sudo npm run dev
```

---
To be able to measure temperature on macOS:

```bash
npm install osx-temperature-sensor --no-save
```